import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';

export default class App extends React.Component {
    render () {
        return (
            <View style={styles.container}>
                <View style={styles.navBar}>
                    <Image source={require('./images/logo.png')} style={{width:98, height:22}}/>
                    <View style={styles.rightNav}>
                        <Image source={require('./images/search.svg')} style={{width:200, height:22}}/>
                    </View>
                </View>
            </View>
        )
    }
}


const styles = StyleSheet.create ({
    container : {
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    },
    navBar: {
        height:55,
        backgroundColor:'white',
        elevation: 3,
        paddingHorizontal:150,
        flexDirection:'row',
        alignItems:'center',   
    },
    rightBar: {

    }
})